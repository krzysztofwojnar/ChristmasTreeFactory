package com.krzysztofwojnar;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static List<ChristmasTree> myTrees = new LinkedList<ChristmasTree>();

    public static void main(String[] args) {
        //ChristmasTree tree = ChristmasTreeFactory.createChristmasTree(TreeSpecies.PINE, 9, 4, 2, 3);
        //((Pine) tree).assemblyTree();
        //((Pine) tree).printTree();
        mainMenu();
        scanner.close();

    }

    public static void mainMenu() {
        int choice;
        while (true) {
            System.out.println("Place an orders for a Christmas tree. Type \n" +
                    "[1] to order a Christmas tree \n" +
                    "[2] to review your order \n" +
                    "[3] to collect your order \n" +
                    "[0] to cancel order");
            if (Main.scanner.hasNextInt()) {
                choice = Main.scanner.nextInt();
            } else {
                System.out.println("Enter a digit.");
                choice = Integer.MAX_VALUE;

            }
            switch (choice) {
                case 1:
                    orderAChristmasTree();
                    break;
                case 2:
                    reviewOrders();
                    break;
                case 3:
                    printAllTrees();
                    return;
                case 0:

                    return;
                default:
                    System.out.println("Incorrect choice type again.");
                    Main.scanner.nextLine();
            }
        }
    }

    public static void orderAChristmasTree() {
        TreeSpecies type = chooseTreeSpecies();
        if (type == null) return;
        int height = 0;
        height = chooseHowManyThings(2,"How tall should be a tree?", Integer.MAX_VALUE/2);
        int segments = 1;
        if (type == TreeSpecies.PINE || type == TreeSpecies.SPRUCE) {
            segments = chooseHowManyThings(1,"How many segments should have a tree?", height-1);
        }
        int lights = 0;
        if (type == TreeSpecies.PINE || type == TreeSpecies.FIR) {
            int maximumAmountOfLights = (int)Math.pow((height-2)-(segments-1), 2) / ChristmasTree.LIGHT.length();
            lights = chooseHowManyThings(0, "How many Christmas Lights should have a tree?", maximumAmountOfLights);
        }
        int maximumAmountOfBulbs = ((int)Math.pow((height-2)-(segments-1), 2) / ChristmasTree.BULB.length()) - ChristmasTree.LIGHT.length() * lights;
        int bulbs = chooseHowManyThings(0, "How many Christmas Bulbs should have a tree?", maximumAmountOfBulbs);
        myTrees.add(ChristmasTreeFactory.createChristmasTree(type, height, bulbs, lights, segments));
    }

    private static TreeSpecies chooseTreeSpecies() {
        String choice;
        Main.scanner.nextLine();
        while (true) {
            System.out.println("Choose a species of a Christmas tree: \n" +
                    "Type \"Fir\", \"Spruce\", \"Pine\", or \"Quit\" to quit");
            choice = Main.scanner.nextLine().toLowerCase();
            if (choice.equals("fir")) {
                return TreeSpecies.FIR;
            } else if (choice.equals("spruce")) {
                return TreeSpecies.SPRUCE;
            } else if (choice.equals("pine")) {
                return TreeSpecies.PINE;
            } else if (choice.equals("quit")) {
                return null;
            }
        }
    }

    private static int chooseHowManyThings(int minimum, String message, int maximum) {
        while (true) {
            System.out.println(message);
            if (Main.scanner.hasNextInt()) {
                int choice = Main.scanner.nextInt();
                if (minimum <= choice && choice <= maximum) {
                    return choice;
                } else if (choice < minimum){
                    System.out.println("Minimum is " + minimum + ". Type again.");
                } else if (maximum  < choice){
                    System.out.println("Maximum is " + maximum + ". Type again.");
                }
            } else {
                System.out.println("Enter a number.");
                Main.scanner.nextLine();
            }
        }

    }

    private static void reviewOrders() {
        for (int i = 0; i < myTrees.size(); i++) {
            switch (myTrees.get(i).getSpecies()) {
                case FIR:
                    System.out.print("Fir");
                    break;
                case SPRUCE:
                    System.out.print("Spruce built from " + myTrees.get(i).getSegments()+ " segments," );
                    break;
                case PINE:
                    System.out.print("Pine built from " + myTrees.get(i).getSegments()+ " segments," );
                    break;
                default:
                    System.out.print("???");
                    break;
            }
            //System.out.print(myTrees.get(i).getClass());
            System.out.print(" " + myTrees.get(i).height + " signs tall, with ");
            if (myTrees.get(i).christmasBulbs > 0) {
                System.out.print(myTrees.get(i).christmasBulbs + " christmas bulbs");
                if (myTrees.get(i).christmasLights > 0)
                    System.out.print(" and " + myTrees.get(i).christmasLights + " christmas lights");
                System.out.println("");
            } else if (myTrees.get(i).christmasLights > 0) {
                System.out.println(myTrees.get(i).christmasLights + " christmas lights");
            } else System.out.println("no decorations");

        }
    }

    private static void printAllTrees() {
        //Iterator i = myTrees.iterator();
        //System.out.println(myTrees.size());
        for (int i = 0; i < myTrees.size(); i++) {
            //System.out.println(myTrees.get(i).segments + " " + myTrees.get(i).height + " " + myTrees.get(i).christmasLights + " " + myTrees.get(i).christmasBulbs);
            myTrees.get(i).printTree();

        }
    }
}
