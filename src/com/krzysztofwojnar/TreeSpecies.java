package com.krzysztofwojnar;

public enum TreeSpecies {
    FIR, PINE, SPRUCE
}
