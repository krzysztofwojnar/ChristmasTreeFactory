package com.krzysztofwojnar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Pine extends ChristmasTree {//sosna

    public Pine(boolean isTest, int height, int segments, int christmasBulbs, int christmasLights) {
        super(isTest, segments, height, christmasBulbs, christmasLights);
        //System.out.println("you have a pine");
        this.assemblyTree();
    }
    public TreeSpecies getSpecies() {
        return TreeSpecies.PINE;
    }
    private List<List> pine = new ArrayList<>();

    public void assemblyTree() {
        assemblyTree(this.height, this.christmasBulbs, this.christmasLights, this.segments);
    }

    public void assemblyTree(int height, int bulbs, int lights, int segments) {
        buildTree(height, segments);
        List<Pair> insepectedTree = this.inspectTree();
        arrangeTree(insepectedTree, bulbs, lights);
    }

    public void buildTree(int height, int segments) {
        //System.out.println(height + " " + segments);
        //int treeArea = 0;
        if (!super.printTree(height, christmasBulbs, christmasLights, segments)) {
            System.out.println("Incorrect parameters");
            return;
        } else {
            int segmentOffset = 0;
            int lastSegmentLine = 0;
            int howOftenSegment = height / segments - 1;
            //System.out.println(howOftenSegment);
            boolean isTopPrinted = false;

            for (int i = 0; i < height; i++) { // jumping to next row
                pine.add(new ArrayList<String>());
                //treeArea = (int) ((double) (2 * ((i + (height - 1)) / 2) + 1) * (height - i - 1) - 2 * (height - i - 1) * ((height - i) % 2) / 2);
                //treeArea -= ((segments - 1) * (segments)) * 2;
                //System.out.println(treeArea);
                boolean isNextSegment = false;
                for (int j = 0; j < 2 * height; j++) { // writing row
                    boolean isInsideTree = ((2 * (height - segments)) / 2 - i <= j) && (j <= (2 * (height - segments)) / 2 + i);
                    if (j == (2 * (height - segments)) / 2 - i) { // tree's left edge or top
                        if (isTopPrinted) {
                            //System.out.print("/");
                            pine.get(i + segmentOffset).add("/");
                        } else {
                            //System.out.print("˄");
                            pine.get(i + segmentOffset).add("˄");
                            isTopPrinted = true;
                            //treeArea--;
                            break;
                        }
                        //treeArea--;

                    } else if (j == (2 * (height - segments)) / 2 + i) { // tree's right edge
                        //System.out.print("\\");
                        pine.get(i + segmentOffset).add("\\");
                        //treeArea--;
                        break;
                    } else if (isInsideTree) { // space inside tree
                        if ((segments > 1 || segments == 1 && i == (height - segments) - 1) && (i == howOftenSegment + lastSegmentLine || i == (height - segments) - 1)) {
                            pine.get(i + segmentOffset).add("_");
                            //treeArea--;
                            //System.out.print("_");
                            isNextSegment = true;
                        } else pine.get(i + segmentOffset).add(" "); //empty space inside tree
                        //treeArea--;
                    } else { // empty space outside tree
                        //System.out.print(" ");
                        pine.get(i + segmentOffset).add(" ");
                    }

                }

                if (isNextSegment) {
                    //pine.add(new ArrayList<String>());
                    lastSegmentLine = i;
                    segments--;
                    height--;
                    segmentOffset++;
                    if (i != height - 1) i--;
                }
                //System.out.println("");

            }
            //System.out.println("");
        }

        // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-leg-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        pine.add(new ArrayList<String>());
        //System.out.println(pine.size());
        for (int j = 0; j < /*spread*/2 * height; j++) { // writing row

            if (j > /*spread*/ (2 * height) / 2 - 1 && j < /*spread*/ (2 * height) / 2 + 1) {
                //System.out.println("*");
                pine.get(pine.size() - 1).add("*");
                break;
            } else {
                pine.get(pine.size() - 1).add(" ");
                //System.out.print(" ");
            }
        }
        //System.out.print(bulbs + "(tree area: ");
        //System.out.println(treeArea + ")");
        //System.out.println(bulbs == 0 || treeArea == 0);
    }

    private List<Pair> inspectTree() {
        //We create true-false map of a pine's array
        List<Pair> treeMap = new ArrayList<Pair>();
        if (pine.size() == 0) System.out.println("There is no tree!");
        Iterator<List> i = pine.iterator();
        int iInt = 0;
        //boolean isInsideTree = false;
        while (i.hasNext()) {
            boolean isInsideTree = false;
            List<String> row = i.next();
            if (row.isEmpty()) break;
            Iterator<String> j = row.iterator();
            int jInt = 0;
            while (j.hasNext()) {
                String symbol = j.next();
                if (symbol.equals("/")) {
                    treeMap.add(new Pair(iInt, jInt));
                    isInsideTree = true;
                } else if (symbol.equals(" ") || symbol.equals("_")) {
                    if (isInsideTree) treeMap.add(new Pair(iInt, jInt));
                } else if (symbol.equals("\\") || symbol.equals("˄")) {
                    treeMap.add(new Pair(iInt, jInt));
                    isInsideTree = false;
                }
                ++jInt;
            }
            ++iInt;
        }

        return treeMap;
    }

    private void arrangeTree(List<Pair> inspectedTree, int christmasBulbs, int christmasLights) {
        Random generator = new Random();
        while (christmasLights > 0) {
            int place = generator.nextInt(inspectedTree.size() - 1);
            //System.out.println(inspectedTree.get(place).row + "; " +  inspectedTree.get(place).position);
            int decorationSize = getDecoration(AllowedDecorations.LIGHT).length();
            if ((inspectedTree.size() - place) >= decorationSize)
                if (inspectedTree.get(place).row == inspectedTree.get(place + (decorationSize - 1)).row) {
                    for (int i = 0; i < decorationSize; i++) {
                        String partOfDecoration = getDecoration(AllowedDecorations.LIGHT).substring(i, i+1);
                        List<String> rowToModify = pine.get(inspectedTree.get(place).row);
                        int positionToReplace = inspectedTree.get(place).position;
                        rowToModify.set(positionToReplace, partOfDecoration);
                        inspectedTree.remove(place);

                    }
                    //System.out.println("elo");
                    christmasLights--;
                }
        }
        while (christmasBulbs > 0) {
            int place = generator.nextInt(inspectedTree.size() - 1);
            //System.out.println(inspectedTree.get(place).row + "; " +  inspectedTree.get(place).position);
            int decorationSize = getDecoration(AllowedDecorations.BULB).length();
            if ((inspectedTree.size() - place) >= decorationSize)
                if (inspectedTree.get(place).row == inspectedTree.get(place + (decorationSize - 1)).row) {
                    for (int i = 0; i < decorationSize; i++) {
                        String partOfDecoration = getDecoration(AllowedDecorations.BULB).substring(i, i+1);
                        List<String> rowToModify = pine.get(inspectedTree.get(place).row);
                        int positionToReplace = inspectedTree.get(place).position;
                        rowToModify.set(positionToReplace, partOfDecoration);
                        inspectedTree.remove(place);

                    }
                    //System.out.println("elo");
                    christmasBulbs--;
                }
        }

    }

    @Override
    public boolean printTree() {
        //System.out.println("pine's printTree()");
        Iterator<List> i = pine.iterator();
        while (i.hasNext()) {
            List<String> row = i.next();
            if (row.isEmpty()) break;
            Iterator<String> j = row.iterator();
            String thisRow = "";
            while (j.hasNext()) {
                thisRow += j.next();
            }
            System.out.println(thisRow);
        }
        return false;
    }

    private class Pair {
        public Pair(int row, int position) {
            this.row = row;
            this.position = position;
        }

        final int row;
        final int position;

    }

}
