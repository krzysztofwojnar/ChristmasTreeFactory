package com.krzysztofwojnar;

import java.util.List;

public interface IRandomArrangement {
    boolean randomArrangement(int spacesToFill, int numberOfElementsToPut);
    void addValuesToReturn (List<Boolean> data);

}
