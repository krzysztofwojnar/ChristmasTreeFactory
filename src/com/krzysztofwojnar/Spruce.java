package com.krzysztofwojnar;

public class Spruce extends ChristmasTree {//świerk

    public Spruce(boolean isTest, int height, int segments, int christmasBulbs, int christmasLights) {
        super(isTest, segments, height, christmasBulbs, christmasLights);
    }

    public Spruce (boolean isTest) {
        super (isTest, 1, 4, 0, 0);
    }

    public TreeSpecies getSpecies() {
        return TreeSpecies.SPRUCE;
    }
    int height;
    int christmasBulbs;
    int christmasLights;
    int segments;


    public boolean printTree () {
        //System.out.println(super.height + " " + super.christmasBulbs + " " + super.segments);
        return printTree(super.height, super.christmasBulbs, super.segments);
    }

    public boolean printTree(int height, int bulbs, int segments) {
        //System.out.println(height + " " + bulbs + " " + segments);
        int treeArea = 0;
        if (!super.printTree(height, christmasBulbs, christmasLights, segments)) {
            System.out.println("Incorrect parameters");
            return false;
        } else {
            int lastSegmentLine = 0;
            int howOftenSegment = height / segments - 1;
            //System.out.println(howOftenSegment);
            boolean isTopPrinted = false;
            for (int i = 0; i < height; i++) { // jumping to next row
                treeArea = (int) ((double)(2*((i+(height-1))/2)+1)*(height - i - 1) - 2*(height-i-1)*((height-i)%2)/2);
                treeArea -= ((segments-1) * (segments))*2;
                //int areaOfTreeInThisRow = 1 + i */*spread*/2;
                boolean isNextSegment = false;
                for (int j = 0; j < /*spread*/2 * height; j++) { // writing row
                    //if (treeArea == 1) {
                    //    System.out.print(bulbs + "(tree area: ");
                    //    System.out.print(treeArea + ")");
                    //}
                    boolean isInsideTree = j >= (2 * (height - segments)) / 2 - i && j <= (2 * (height - segments)) / 2 + i;
                    if (isInsideTree && getArrangement().randomArrangement(treeArea, bulbs)) {

                        bulbs--;
                        if (!isTopPrinted) {
                            System.out.print("*");
                            isTopPrinted = true;
                        } else System.out.print(ChristmasTree.getDecoration(AllowedDecorations.BULB));
                        if ((segments > 1 || segments == 1 && i == (height - segments) - 1) && (i == howOftenSegment + lastSegmentLine || i == (height - segments) - 1)) isNextSegment = true;
                        treeArea--;
                        continue;
                    }
                    if (j == /*spread*/ (2 * (height - segments)) / 2 - i) { // tree's left edge or top
                        if (isTopPrinted) {
                            System.out.print("/");
                        } else {
                            System.out.print("˄");
                            isTopPrinted = true;
                            treeArea--;
                            break;
                        }
                        treeArea--;

                    } else if (j == (/*spread*/ 2 * (height - segments)) / 2 + i) { // tree's right edge
                        System.out.print("\\");
                        treeArea--;
                        break;
                    } else if (isInsideTree) { // space inside tree
                        if ((segments > 1 || segments == 1 && i == (height - segments) - 1) && (i == howOftenSegment + lastSegmentLine || i == (height - segments) - 1)) {
                            System.out.print("_");
                            isNextSegment = true;
                        } else System.out.print(" "); //empty space inside tree
                        treeArea--;
                    } else { // empty space outside tree
                        System.out.print(" ");
                    }

                }
                if (isNextSegment) {
                    lastSegmentLine = i;
                    segments--;
                    height--;
                    if (i != height - 1) i--;
                }
                System.out.println("");

            }
            //System.out.println("");
        }
        for (int j = 0; j < /*spread*/2 * height; j++) { // writing row
            if (j > /*spread*/ (2 * height) / 2 - 1 && j < /*spread*/ (2 * height) / 2 + 1) {
                System.out.println("*");
                break;
            } else {
                System.out.print(" ");
            }
        }
        //System.out.print(bulbs + "(tree area: ");
        //System.out.println(treeArea + ")");
        return bulbs == 0 || treeArea == 0;
    }
}

