package com.krzysztofwojnar;

public class ChristmasTreeFactory {
    public static ChristmasTree createChristmasTree (TreeSpecies species, int height, int christmasBalls, int christmasLights, int segments) {
        switch (species) {
            case FIR:
                return new Fir(false, height,christmasBalls,christmasLights);
            case SPRUCE:
                return new Spruce(false, height, segments, christmasBalls,christmasLights);
            case PINE:
                return new Pine(false, height, segments, christmasBalls, christmasLights);
            default:
                return null;
        }
    }
}
