package com.krzysztofwojnar;

import java.util.ArrayList;
import java.util.List;

public class RandomArrangementForTesting implements IRandomArrangement {
    private List dataForTesting = new ArrayList<List>();
    public void addValuesToReturn (List<Boolean> data) {
        dataForTesting.add(data);
    }
    public static int currentTestNumber = 0;

    public boolean randomArrangement(int spacesToFill, int numberOfElementsToPut) {
        if (spacesToFill<=numberOfElementsToPut) return true;

        return numberOfElementsToPut >= 1;
    }
}
