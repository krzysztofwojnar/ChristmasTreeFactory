package com.krzysztofwojnar;


import java.util.ArrayList;
import java.util.List;

public abstract class ChristmasTree {
    public static final String BULB = "O";
    public static final String LIGHT = "[*]";
    public ChristmasTree(boolean isTest, int segments, int height, int christmasBulbs, int christmasLights) {
        if (!isTest) {
            this.arrangement = new RandomArrangement();
        } else this.arrangement = new RandomArrangementForTesting();
        this.segments = segments;
        this.height = height;
        this.christmasBulbs = christmasBulbs;
        this.christmasLights = christmasLights;
    }
    public abstract TreeSpecies getSpecies();
    public int getSegments() {
        return this.segments;
    }
    protected int segments;
    protected int height;
    protected int christmasBulbs;
    protected int christmasLights;
    public static String getDecoration(AllowedDecorations decorations) {
        switch (decorations) {
            case BULB:
                return ChristmasTree.BULB;
            case LIGHT:
                return ChristmasTree.LIGHT;
        }
        return null;
    }
    public IRandomArrangement getArrangement() {
        return arrangement;
    }

    private IRandomArrangement arrangement;

    /* exercise
    public class Row{
        public Row(int number) {
            this.number = number;
            this.StartOfTreeArea = -1;
        }

        private int number;
        private Integer StartOfTreeArea;
        private List<String> row = new ArrayList<>();
        public void addChar(String nextChar) {
            if (nextChar.length() == 1) row.add(nextChar);
        }
        public String getChar (int position) {
            if (position >= row.size() || position < 0) {
                System.out.println("Wrong position!");
                return "-1";
            }
            return row.get(position);
        }
        public String replaceChar (int position, String newChar) { // replaces string by a new one. Returns currently holded string
            if (position >= row.size() || position < 0) {
                System.out.println("Wrong position!");
                return "-1";
            }
            String oldContent = row.get(position);
            if (newChar.length() == 1) row.set(position, newChar);
            return oldContent;
        }
        public void removeRow () {
            row.clear();
        }
        public int getStartOfTreeArea() {
            return this.StartOfTreeArea;
        }
    }*/

    public abstract boolean printTree();
    public boolean printTree(int height, int christmasBulbs, int christmasLights, int segments) {
        if (height <= 0 || christmasBulbs < 0 || christmasLights < 0 || segments < 1) {
            System.out.println("Incorrect parameters");
            return false;
        } else {

        }
        return true;
    }
}
