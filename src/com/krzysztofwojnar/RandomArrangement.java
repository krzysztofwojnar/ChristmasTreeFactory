package com.krzysztofwojnar;

import java.util.List;

public class RandomArrangement implements IRandomArrangement {
    public boolean randomArrangement(int pool, int randomElements) {
        if (pool <= 0 || randomElements <= 0) return false;
        if (pool==randomElements) return true;
        int random = (int) (Math.random() *pool);
        //if (random <= randomElements) System.out.print(randomElements + "—" + pool + " ");
        return random <= randomElements;
    }
    public void addValuesToReturn (List<Boolean> data) {
        System.out.println("Cannot add values to return! Returned values are random!");
    }
}
