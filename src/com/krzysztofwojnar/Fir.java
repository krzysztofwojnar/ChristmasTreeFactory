package com.krzysztofwojnar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Fir extends ChristmasTree {//jodła

    public static final String LIGHT = "[i]";

    private int areaOfTreeInThisRow;
    List<List> fir = new ArrayList<>();


    public Fir(boolean isTest, int height, int christmasBulbs, int christmasLights) {
        super(isTest, 1, height, christmasBulbs, christmasLights);
        this.assemblyTree();
    }
    public TreeSpecies getSpecies() {
        return TreeSpecies.FIR;
    }
    public static String getDecoration(AllowedDecorations decorations) {
        switch (decorations) {
            case BULB:
                return Fir.BULB;
            case LIGHT:
                return Fir.LIGHT;
        }
        return null;
    }
    public void assemblyTree(int height, int christmasBulbs, int christmasLights) {
        this.height = height;
        this.christmasBulbs = christmasBulbs;
        this.christmasLights = christmasLights;
        this.assemblyTree();
    }

    ;

    public void assemblyTree() {
        int christmasBulbs = this.christmasBulbs;
        int christmasLights = this.christmasLights;
        int decorationFields = 0;
        decorationFields += christmasBulbs * Fir.BULB.length();
        decorationFields += christmasLights * Fir.LIGHT.length();
        int treeArea = 0;
        boolean topWasPrinted = false;
        for (int i = 0; i < height - 1; i++) { // jumping to next row
            treeArea = (int) ((double) (2 * ((i + (height - 1)) / 2) + 1) * (height - i - 1) - 2 * (height - i - 1) * ((height - i) % 2) / 2);
            //int areaOfTreeInThisRow = 1 + i *2;
            fir.add(new ArrayList<String>());
            for (int j = 0; j < 2 * height; j++) { // writing row


                if (j >= (2 * (height - 1)) / 2 - i && j <= (2 * (height - 1)) / 2 + i) {
                    if (getArrangement().randomArrangement(treeArea, decorationFields)) {
                        if (getArrangement().randomArrangement(decorationFields, christmasBulbs)) {
                            if (christmasBulbs >= 0 && j <= (2 * height) / 2 + i - Fir.BULB.length()) {
                                fir.get(i).add(Fir.BULB);
                                christmasBulbs--;
                                j += Fir.BULB.length() - 1;
                                treeArea -= Fir.BULB.length();
                                decorationFields -= Fir.BULB.length();
                            } else fir.get(i).add("*");
                        } else {
                            if (christmasLights >= 0 && j <= (2 * height) / 2 + i - Fir.LIGHT.length()) {
                                fir.get(i).add(Fir.LIGHT);
                                christmasLights--;
                                j += Fir.LIGHT.length() - 1;
                                decorationFields -= Fir.LIGHT.length();
                                treeArea -= Fir.LIGHT.length();
                            } else fir.get(i).add("*");
                        }

                    } else fir.get(i).add("*");
                    treeArea--;


                    //System.out.print("*");
                } else {
                    if (j == (2 * height) / 2 + i) break;
                    fir.get(i).add(" ");
                    //System.out.print(" ");
                }
            }
            //System.out.println("");
        }
        fir.add(new ArrayList<String>());
        for (int j = 0; j < /*spread*/2 * height; j++) { // writing row
            if (j >= /*spread*/ (2 * (height)) / 2 - 1 && j <= /*spread*/ (2 * (height)) / 2 + 1) {
                //System.out.println("*");
                fir.get(height - 1).add("*");
                break;
            } else {
                //System.out.print(" ");
                fir.get(height - 1).add(" ");
            }
        }
        //System.out.println(decorationFields + " tree area: " + treeArea);
    }

    public boolean printTree() {
        Iterator<List> i = fir.iterator();
        while (i.hasNext()) {
            List<String> row = i.next();
            if (row.isEmpty()) break;
            Iterator<String> j = row.iterator();
            String thisRow = "";
            while (j.hasNext()) {
                thisRow += j.next();
            }
            System.out.println(thisRow);

        }
        return false;
    }
}
